#include <malloc.h>
#include "linked_list.h"
#include "ho_functions.h"

void foreach(linked_list_t* start, void (*func)(int)){
	size_t i;
	size_t length=list_length(start);
	for (i=0;i<length;i++){
		func(list_get(start,i));
	}
}

linked_list_t* map(int (*func)(int), linked_list_t* start){
	size_t i;
	size_t length=list_length(start);
	linked_list_t* new_pointer=list_create(func(list_get(start,0)));
	for (i=1;i<length;i++){
		list_add_back(func(list_get(start,i)),new_pointer);
	}
	return new_pointer;
}

void map_mut(int (*func)(int), linked_list_t* start){
	size_t i;
	size_t length=list_length(start);
	for (i=0;i<length;i++){
		list_change_value(start,i,func(list_get(start,i)));
	}
}

int foldl(int rax,int (*func)(int,int),linked_list_t* start){
	size_t i;
	size_t length=list_length(start);
	for (i=0;i<length;i++){
		rax=func(rax,list_get(start,i));
	}
	return rax;
}

linked_list_t* iterate(int s,int n,int (*func)(int)){
	size_t i;
	linked_list_t* new_pointer=list_create(s);
	for(i=1;i<n;i++){
		s=func(s);
		new_pointer=list_add_front(s,new_pointer);
	}
	return new_pointer;
}