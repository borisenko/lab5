#ifndef _HO_FUNCTIONS_H_
#define _HO_FUNCTIONS_H_
void foreach(linked_list_t* start,void (*func)(int));
linked_list_t* map(int (*func)(int), linked_list_t* start);
void map_mut(int (*func)(int), linked_list_t* start);
int foldl(int rax,int (*func)(int,int),linked_list_t* start);
linked_list_t* iterate(int s,int n,int (*func)(int));
#endif