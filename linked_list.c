#include <malloc.h>
#include "linked_list.h"

struct linked_list_t{
	int value;
	struct linked_list_t* next;
};

linked_list_t* list_create(int x){
	return list_add_front(x,NULL);
}

linked_list_t* list_add_front(int x, linked_list_t* pointer){
	linked_list_t* new_node=(linked_list_t*) malloc(sizeof(linked_list_t));
	new_node->value=x;
	new_node->next=pointer;
	return new_node;
}

void list_add_back(int x, linked_list_t* pointer){
	while (pointer->next!=NULL){
		pointer=pointer->next;
	}
	pointer->next=list_add_front(x,NULL);
}

int list_get(linked_list_t* pointer,size_t i){
	linked_list_t* node=list_node_at(pointer,i);
	if (node==NULL) return 0;
	else return node->value;
}

void list_free(linked_list_t* pointer){
	while (pointer!=NULL){
		linked_list_t* next=pointer->next;
		free(pointer);
		pointer=next;
	}
}

size_t list_length(linked_list_t* pointer){
	size_t counter=0;
	while (pointer!=NULL){
		counter++;
		pointer=pointer->next;
	}
	return counter;
}

linked_list_t* list_node_at(linked_list_t* pointer,size_t i){
	size_t j;
	for (j=0;j<i;j++){
		if (pointer->next==NULL) return NULL;
		pointer=pointer->next;
	}
	return pointer;
}

long list_sum(linked_list_t* pointer){
	long sum=0;
	while(pointer!=NULL){
		sum+=pointer->value;
		pointer=pointer->next;
	}
	return sum;
}

void list_change_value(linked_list_t* pointer,size_t i,int value){
	linked_list_t* node=list_node_at(pointer,i);
	node->value=value;
}
