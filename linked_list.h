#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_
struct linked_list_t;
typedef struct linked_list_t linked_list_t;

linked_list_t* list_create(int);
linked_list_t* list_add_front(int, linked_list_t*);
void list_add_back(int, linked_list_t*);
int list_get(linked_list_t*,size_t);
void list_free(linked_list_t*);
size_t list_length(linked_list_t*);
linked_list_t* list_node_at(linked_list_t*,size_t);
long list_sum(linked_list_t*);
void list_change_value(linked_list_t*,size_t,int);
#endif