#include <stdio.h>
#include <limits.h>
#include <math.h>
#include "linked_list.h"
#include "ho_functions.h"

static linked_list_t* read_nodes(FILE* file){
	int value;
	linked_list_t* pointer=NULL;
	if (file==NULL){
		puts("Файл не найден");
		return NULL;
	}
	while(fscanf(file,"%d",&value)!=EOF){
		if (pointer==NULL) pointer=list_create(value);
		else pointer=list_add_front(value,pointer);
	}
	return pointer;
}

static void print_value_with_space(int x){
	printf("%d ",x);
}

static void print_value_with_newline(int x){
	printf("%d\n",x);
}

static int squareOfNumber(int x){
	if (x*x>INT_MAX) return 0;
	return x*x;
}

static int cubeOfNumber(int x){
	if (x*x*x>INT_MAX || x*x*x<INT_MIN) return 0;
	return x*x*x;
}

static int sum(int a,int b){
	if ((a+b)>INT_MAX || (a+b)<INT_MIN) return 0;
	return a+b;
}

static int maxValue(int a,int b){
	if (a>=b) return a;
	return b;
}

static int minValue(int a,int b){
	if (b<=a) return b;
	return a;
}

int abs(int a);

static int mul2(int a){
	if (a*2>INT_MAX || a*2<INT_MIN) return 0;
	return a*2;
}

int main(int args,char** argv){
	FILE* file;
	linked_list_t* pointer,*new_pointer;
	if (args>2){
		puts("Допустимо задать только один файл с числами");
		return -1;
	}
	if (args==2){
		file=fopen(argv[1],"r");
		if (file==NULL) {
			puts("Файл не был найден. Задайте числа из командной строки или завершите выполнение.");
			file=stdin;
		}
	} else {
		puts("Введите через пробел целые числа:");
		file=stdin;
	}
	pointer=read_nodes(file);
	
	puts("Список через пробел:");
	foreach(pointer,print_value_with_space);
	puts("\nСписок с новой строки:");
	foreach(pointer,print_value_with_newline);
	
	new_pointer=map(squareOfNumber,pointer);
	puts("Квадраты чисел:");
	foreach(new_pointer,print_value_with_space);
	list_free(new_pointer);
	puts("\nКубы чисел:");
	new_pointer=map(cubeOfNumber,pointer);
	foreach(new_pointer,print_value_with_space);
	list_free(new_pointer);
	
	printf("\nСумма элементов списка:%d\n",foldl(0,sum,pointer));
	printf("Максимальный элемент списка:%d\n",foldl(INT_MIN,maxValue,pointer));
	printf("Минимальный элемент списка:%d\n",foldl(INT_MAX,minValue,pointer));
	
	map_mut(abs,pointer);
	puts("Модули элементов:");
	foreach(pointer,print_value_with_space);
	
	new_pointer=iterate(2,10,mul2);
	puts("\nПервые десять чисел степеней 2:");
	foreach(new_pointer,print_value_with_space);
	list_free(new_pointer);
	
	list_free(pointer);
	return 0;
}