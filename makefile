all: main

main: linked_list.c ho_functions.c main.c
	gcc -o main -ansi -pedantic-errors -Wall -Werror linked_list.c ho_functions.c main.c

clean:
	rm -f main